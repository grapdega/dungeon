# Dungeon game documentation
## Genel Bakış
******************************************
### Oyun İsmi : Dungeons (Değişecek)

Oyun point-and-click adventure tarzında bulmaca çözüp karşımıza çıkan düşmanları kestiğimiz bir oyun. Yapı olarak 3d izometrik yapıda olacak. Oynayanı atmosferinde tutabilecek şirinlikte grafik animasyon ve efektlere sahip olacak. Toon bir yapıda shader kullanılacak(hyper casual kadar göz yormuyacak renk paleti ile).

## Hikaye

Sürekli aynı hayatı yaşıyan karakterimizin zamanla piskolojisi bozuluyor.Sürekli hayıflanan çevresindeki insanlar tarafından mırıltılar duyar. Bir gün işten evine geldiğinde evinde normalde olmayan bir obje görür. Sabah uyanır salona geçtiği vakit masanın üzerinde farklı bir obje görür eline aldığında farklı bir evrene geçiş yapar. (Bu sırada mırıltılar şiddetlenerek devam etmektedir). Evrende herşey terse dönmüştürtür neredeyse beyaz bir dünya içerisine girmiştir. Dışarı çıkar ve macerasına başlar(Burası doldurulacak).Bulundu yer aslında karakterimizin beynindeki düşünceler. Bu bulunduğu ortamdan çıkmak için kaleye varması gerekmektedir(Kale aslında beynindeki bu piskolojik sorunların kaynağıdır).

## Referans Oyunlar
* Nox Game
* machinarium(Atmosfer yapısı)
* gemotry dash (level yapısı)
* half-life (bulmaca mantığı)
* where cards fall (shader yapısı)
* superliminal (level yapısı)
