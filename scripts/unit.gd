extends KinematicBody

var gravity = preload("res://scripts/gravity.gd").new()
var move = preload("res://scripts/move.gd").new()

var node = null

export var speed = 10
export var gravity_power = 10
export var jump_power = 5
export var can_jump = true

func _init_node():
	if not node:
		node = self
		move.set_node(node)
		gravity.set_node(node)
		move.speed = speed
		gravity.jump_power = jump_power
		gravity.gravity = gravity_power
		gravity.can_jump = can_jump
	
func _process(delta):
	_init_node()
	_update(delta)

func _update(delta):
	move.process(delta)
	gravity.process(delta)
