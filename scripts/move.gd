extends Node
var _node = null
var _velocity = Vector3.ZERO
var speed = 10

func set_node(new_node):
	_node = new_node


func process(delta):
	if not _node:
		return
	if Input.is_action_pressed("ui_left"):
		_velocity.x = speed*delta*100
	elif Input.is_action_pressed("ui_right"):
		_velocity.x = -1*speed*delta*100
	else:
		_velocity.x = 0
	
	if Input.is_action_pressed("ui_up"):
		_velocity.z = speed*delta*100
	elif Input.is_action_pressed("ui_down"):
		_velocity.z = -1*speed*delta*100
	else:
		_velocity.z = 0
	_velocity = _node.move_and_slide(_velocity,Vector3.UP)
