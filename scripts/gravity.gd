extends Node

var _node = null
var gravity = 10
var jump_power = 5
var _velocity = Vector3.ZERO
var can_jump = true

# grv_jump is jump power. 
var grv_jump = 0

func set_node(new_node):
	_node = new_node

func process(delta):
	if not _node:
		return
	_velocity.y -= gravity * delta*4
	if _velocity.y < -1*gravity*4:
		_velocity.y = -1*gravity*4
		grv_jump = 0
	if grv_jump > 0:
		_velocity.y += gravity/2
		grv_jump -=1
	if _velocity.y < 0.001 and _velocity.y > -0.001:
		_velocity.y = 0
	_velocity = _node.move_and_slide(_velocity,Vector3.UP)
	if Input.is_action_just_pressed("ui_accept"):
		jump()

func jump():
	if not can_jump:
		return
	if _velocity.y > 0.001 or _velocity.y < -0.001:
		return
	if grv_jump != 0:
		return
	print("jump")
	if grv_jump < jump_power*(10/gravity):
		grv_jump += jump_power*(10/gravity)
